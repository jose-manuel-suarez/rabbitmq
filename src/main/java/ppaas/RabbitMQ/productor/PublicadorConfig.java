package ppaas.RabbitMQ.productor;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PublicadorConfig {
	
	@Value("${rabbitmq.queue.name}")
	private String mensaje;

	@Bean
	public Queue queue() {
		return new Queue(mensaje);
	}

}
