package ppaas.RabbitMQ.productor;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import ppaas.RabbitMQ.controller.Data;

@Component
@EnableRabbit
@Slf4j
public class Productor {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private Queue queue;

	@PostConstruct
	public void setProperty() {
		System.setProperty("spring.amqp.deserialization.trust.all", "true");
	}

	public void enviar(
		Data datos
	) {
		log.info("Productor enviando mensaje hacia: '{}' -> {}", queue.getName(), datos);
        rabbitTemplate.convertAndSend(queue.getName(), datos);
    }
	
}
