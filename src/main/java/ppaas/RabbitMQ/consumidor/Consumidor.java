package ppaas.RabbitMQ.consumidor;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import ppaas.RabbitMQ.controller.Data;

@Slf4j
@Component
public class Consumidor {
	
	@Value("${rabbitmq.queue.name: [NO_NAME]}")
	private String cola;
	
	private void demorar(int mills) {
		try {
			Thread.sleep(mills);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@RabbitListener(queues = "${rabbitmq.queue.name}")
	public void receive(
		@Payload Data datos
	) {
		log.info("Consumidor recibiendo mensaje desde: '{}' -> {}", cola, datos);
		demorar(10000);
	}

}
