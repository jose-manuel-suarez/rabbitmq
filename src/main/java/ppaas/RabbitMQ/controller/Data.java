package ppaas.RabbitMQ.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.io.Serializable;

@AllArgsConstructor
@Getter
@ToString
public class Data implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final long id;

	private String timestamp;

	private final String mensaje;

}
