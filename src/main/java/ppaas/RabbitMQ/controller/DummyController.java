package ppaas.RabbitMQ.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ppaas.RabbitMQ.service.DummyService;

@RestController
@RequestMapping("rabbitmq")
public class DummyController {
	
	@Autowired
	private DummyService servicio;

	@GetMapping({"/enviar","/enviar/{mensajeParam}"})
	public void probarEnvioDeMensaje(
		@PathVariable(required = false) String mensajeParam
	) {
		final String mensaje = (mensajeParam != null ? mensajeParam : "[EMPTY]");	
		servicio.enviarARabbitMQ(mensaje);
	}

}
