package ppaas.RabbitMQ.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppaas.RabbitMQ.controller.Data;
import ppaas.RabbitMQ.productor.Productor;

@Service
public class DummyService {
	
	@Autowired
	private Productor productor;

	public void enviarARabbitMQ(
		String mensaje
	) {
		Data datos = new Data(
			ThreadLocalRandom.current().nextInt(),
			DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL, FormatStyle.MEDIUM).format(LocalDateTime.now()),
			mensaje
		);

		productor.enviar(datos);
	}

}
